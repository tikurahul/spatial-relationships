package com.rahulrav

import com.esri.core.geometry.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.*

class Convert {
  companion object {
    public fun loadPolygons(): JSONArray? {
      val stream = Thread.currentThread().contextClassLoader.getResourceAsStream("tz_world.geojson")
      val reader = BufferedReader(InputStreamReader(stream))
      try {
        val contents = reader.readText()
        return JSONObject(contents).optJSONArray("features")
      } finally {
        reader.close()
      }
    }
  }
}

fun main(args: Array<String>) {
  val output = BufferedWriter(FileWriter("/Users/rahulrav/Downloads/tz_world.json"))
  val states = Convert.loadPolygons()
  if (states != null) {
    val length = states.length()
    val result = JSONObject()
    val features = JSONArray()
    for (i in 0..length-1) {
      val state = states.optJSONObject(i)
      val geometry = state.getJSONObject("geometry")
      val properties = state.getJSONObject("properties")
      val polygon = OperatorImportFromGeoJson.local().execute(
          GeoJsonImportFlags.geoJsonImportDefaults,
          Geometry.Type.Polygon,
          geometry.toString(),
          null
      )
      val esriGeometry = OperatorExportToJson.local().execute(SpatialReference.create(4326), polygon.geometry)
      val json = JSONObject()
      json.put("geometry", JSONObject(esriGeometry))
      json.put("attributes", properties)
      features.put(i, json)
    }
    result.put("features", features)
    output.write(result.toString(2))
    output.close()
  }
}
