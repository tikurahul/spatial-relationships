package com.rahulrav

import com.esri.core.geometry.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader

class Main {
  companion object {

    public fun loadPolygons(): JSONArray? {
      val stream = Thread.currentThread().contextClassLoader.getResourceAsStream("Province.json")
      val reader = BufferedReader(InputStreamReader(stream))
      try {
        val contents = reader.readText()
        return JSONObject(contents).optJSONArray("features")
      } finally {
        reader.close()
      }
    }

    //http://www.gal-systems.com/2011/07/convert-coordinates-between-web.html
    public fun toWebMercator(input: Point): Point? {
      val lon = input.x
      var lat = input.y
      if (Math.abs(lon) > 180 || Math.abs(lat) > 90) {
        return null
      } else {
        val n = lon * 0.017453292519943295
        val x = 6378137.0 * n
        val angle = lat * 0.017453292519943295
        val y = 3189068.5 * Math.log((1.0 + Math.sin(angle)) / (1.0 - Math.sin(angle)))
        return Point(x, y)
      }
    }

  }
}

fun main(args: Array<String>) {
  val point = Point(-79.3892455, 43.6425662)
  val states = Main.loadPolygons()
  if (states != null) {
    val length = states.length()
    for (i in 0..length-1) {
      val state = states.optJSONObject(i)
      val geometry = state.getJSONObject("geometry")
      val polygon = OperatorImportFromJson.local().execute(Geometry.Type.Polygon, geometry)
      if (OperatorIntersects.local().execute(polygon.geometry, Main.toWebMercator(point), SpatialReference.create(102100), null)) {
        println("Point intersects $state")
        break
      }
    }
  }
}
